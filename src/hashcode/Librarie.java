/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashcode;

import java.util.ArrayList;

/**
 *
 * @author Kilian
 */
public class Librarie {
    int id;
    ArrayList<Book> booksList = new ArrayList<>();
    ArrayList<Book> booksSended = new ArrayList<>();
    boolean signed;
    int numScanPerDay;
    int signProcessTime;
    int lastBookScannedPos = 0;
    
    public Librarie(int id, boolean signed, int numScanPerDay, int signProcessTime) {
        this.id = id;
        this.signed = signed;
        this.numScanPerDay = numScanPerDay;
        this.signProcessTime = signProcessTime;
    }

    public int scan() {
        int sumScore = 0;
        for (int i = lastBookScannedPos; i < numScanPerDay; i++) {
            Book b = booksList.get(i);
            booksSended.add(b);
            sumScore += b.score;
        }
        return sumScore;
    }
    
    
   
}
